
const AWS = require( 'aws-sdk' );
AWS.config.update({ region: 'us-east-1' });

var sns = new AWS.SNS();

var params = {
    TargetArn:'arn:aws:sns:us-east-1:103346953322:MartySNSTopic',
    Message:'Testing the fan out.',
    Subject: 'Testing SNS from Node.js'
};

sns.publish(params, function(err,data){
  console.log(err, data.MessageId);
});
