//
// Name: firehoseDSJob.js
// Auth: Martin Burolla
// Date: 6/5/2018
// Desc: Uploads a record to a Firehose Delivery stream every five seconds.
//

const AWS = require( 'aws-sdk' );
AWS.config.update({ region: 'us-east-1' });

function runIt() {

  const fireHose = new AWS.Firehose();

  const data = {
    message: 'This is my data. Hello world.',
    customerId: 1, 
    sales: 100.00
  }

  const params = {
    DeliveryStreamName: 'MartyStream1', 
    Record: { Data: new Buffer(JSON.stringify(data)) }
  }

  fireHose.putRecord(params, (err, data) => {
    console.log(data.RecordId);
  });
}

const intervalObj = setInterval(() => {
  runIt();
}, 5000);
